﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTwo : MonoBehaviour
{
    public int iLevelLoad;
    public string sLevelLoad;

    public float LoadDelay = 2f;

    public bool UseIntegerLoad = false;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject collisionGameObject = collision.gameObject;

        if (collisionGameObject.name == "Player")
        {
            LoadScene();
        }
    }

    void LoadScene()
    {
        if (UseIntegerLoad)
        {
            SceneManager.LoadScene(iLevelLoad);
        }
        else
        {
            SceneManager.LoadScene(sLevelLoad);
        }
    }
}
