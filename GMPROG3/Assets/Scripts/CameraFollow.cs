﻿
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Target;
    public Vector2 Offset;
    public float Speed;

    private void Update()
    {
        Follow();
    }

    void Follow()
    {
        Vector3 targetPosition = Target.position;
        targetPosition.x += Offset.x;
        targetPosition.y += Offset.y;
        targetPosition.z = transform.position.z;
        Vector3 cameraPosition = Vector3.Lerp(transform.position, targetPosition, Speed * Time.deltaTime);
        transform.position = targetPosition;
    }
}