﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Health Health;


    Rigidbody2D rb;
    public float Speed;
    public float JumpForce;
    bool isGrounded = false;
    public Transform isGroundedChecker;
    public float CheckGroundRadius;
    public LayerMask GroundLayer;
    public float FallMultiplier = 2.5f;
    public float LowJumpMultiplier = 2f;
    public float RememberGroundedFor;
    float LastTimeGrounded;
    public int DefaultAdditionalJumps = 1;
    int AdditionalJumps;

    public bool FacingRight = true;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
        BetterJump();
        Checker();

        if (rb.position.y < -2f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
    }

    void Move()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float RunSpeed = x * Speed;
        rb.velocity = new Vector2(RunSpeed, rb.velocity.y);

        if (x < 0 && FacingRight)
        {
            Flip();
        }
        else if (x > 0 && !FacingRight)
        {
            Flip();
        }
    }

    void Jump()
    {
        //Debug.Log("is grounded "+ isGrounded);
        if (Input.GetKeyDown(KeyCode.Space) && (isGrounded || Time.time - LastTimeGrounded <= RememberGroundedFor || AdditionalJumps > 0))
        {
            rb.velocity = new Vector2(rb.velocity.x, JumpForce);
            AdditionalJumps--;
            //Debug.Log("jump");
        }
    }

    void Checker()
    {
        Collider2D colliders = Physics2D.OverlapCircle(isGroundedChecker.position, CheckGroundRadius, GroundLayer);
        if (colliders != null)
        {
            isGrounded = true;
            AdditionalJumps = DefaultAdditionalJumps;
        }
        else
        {
            if (isGrounded)
            {
                LastTimeGrounded = Time.time;
            }
            isGrounded = false;
        }
    }

    void BetterJump()
    {
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity * (FallMultiplier - 1) * Time.deltaTime;
            //Debug.Log("Betterjump1");
        }
        else if (rb.velocity.y > 0 && !Input.GetKey(KeyCode.Space))
        {
            rb.velocity += Vector2.up * Physics2D.gravity * (LowJumpMultiplier - 1) * Time.deltaTime;
            //Debug.Log("Betterjump2");
        }
    }

    void Flip()
    {
        FacingRight = !FacingRight;
        transform.Rotate(Vector3.up * 180);
    }

}
