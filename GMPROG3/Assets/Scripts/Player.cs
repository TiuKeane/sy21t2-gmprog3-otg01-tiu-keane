﻿
using UnityEngine;

public class Player : MonoBehaviour
{
    public PlayerMovement Movement;

    void OnCollisionEnter (Collision CollisionInfo)
    {
        if (CollisionInfo.collider.tag == "Lava")
        {
            Movement.enabled = false;
            FindObjectOfType<GameManager>().EndGame();
        }
    }
}
