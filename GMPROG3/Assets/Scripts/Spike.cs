﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    public AudioSource HurtSound;
    public int DamageValue = 20;

    void Start()
    {
        HurtSound = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            HurtSound.Play();
            collision.gameObject.GetComponent<Health>().TakeDamage(DamageValue);
        }
    }
}
