﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    public AudioSource EatingSound;

    public void PlaySoundEffect()
    {
        EatingSound.Play();
    }
}
