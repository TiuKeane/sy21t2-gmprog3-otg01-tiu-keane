﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealHealth : UnityEvent<int> { }
public class DamageHealth : UnityEvent<int> { }

public class Health : MonoBehaviour
{
    public HealHealth EvtHeal = new HealHealth();
    public DamageHealth EvtDamage = new DamageHealth();

    public int MaxHealthValue;
    public int HealthValue;

    public void Heal(int value)
    {
        HealthValue += value;

        if (HealthValue > MaxHealthValue)
        {
            HealthValue = MaxHealthValue;
        }

        EvtHeal.Invoke(HealthValue);
    }

    public void TakeDamage(int value)
    {
        HealthValue -= value;

        if (HealthValue < 0)
        {
            HealthValue = 0;
            if (HealthValue == 0)
            {
                FindObjectOfType<GameManager>().EndGame();
            }
        }

        EvtDamage.Invoke(HealthValue);

    }
}
