﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public AudioSource HurtSound;

    public int DamageValue = 1000;

    void Start()
    {
        HurtSound = GetComponent<AudioSource>();
    }
      

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            HurtSound.Play();
            collision.gameObject.GetComponent<Health>().TakeDamage(DamageValue);
            
        }
    }
}
