﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarScript : MonoBehaviour
{
    public Health Health;

    public Slider Slider;
    public Gradient gradient;
    public Image Fill;

    void Start()
    {
        SetMaxHealth(Health.MaxHealthValue);

        Health.EvtHeal.AddListener(SetHealth);
        Health.EvtDamage.AddListener(SetHealth);
    }

    void Update()
    {
        DamageOverTime(1, 1000);
    }

    public void SetMaxHealth(int health)
    {
        Slider.maxValue = health;
        Slider.value = health;
        Fill.color = gradient.Evaluate(1f);
    }

    public void SetHealth(int health)
    {
        Slider.value = health;
        Fill.color = gradient.Evaluate(Slider.normalizedValue);
    }

    public void DamageOverTime(float DamageAmount, float DamageTime)
    {
        StartCoroutine(DamageOverTimeCoroutine(DamageAmount, DamageTime));
    }

    IEnumerator DamageOverTimeCoroutine(float DamageAmount, float Duration)
    {
        float AmountDamaged = 0;
        float DamagePerLoop = DamageAmount / Duration;
        while (AmountDamaged < DamageAmount)
        {
            Slider.value -= DamagePerLoop;
            Debug.Log(Slider.value.ToString());
            //AmountDamaged += DamagePerLoop;
            yield return new WaitForSeconds(1f);

            if (Slider.value == 0)
            {
                FindObjectOfType<GameManager>().EndGame();
            }
        }
    }
}
