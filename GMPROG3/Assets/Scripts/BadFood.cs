﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadFood : MonoBehaviour
{
    public int DamageValue = 20;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Health>().TakeDamage(DamageValue);
            Destroy(gameObject);
        }
    }
}
